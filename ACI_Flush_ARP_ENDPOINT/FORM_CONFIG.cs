﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Configuration;
using System.Collections.Specialized;

namespace ACI_Flush_ARP_ENDPOINT
{
    public partial class FORM_CONFIG : Form
    {
        string apic_id = ConfigurationManager.AppSettings.Get("APIC_ID");
        string apic_pw = ConfigurationManager.AppSettings.Get("APIC_PASS");
        string line_token = ConfigurationManager.AppSettings.Get("Line_token");
        int SDI_AUTO_TIMER = int.Parse(ConfigurationManager.AppSettings.Get("SDI_AUTO_TIMER"));
        int SDI_AUTO_FLUSH = int.Parse(ConfigurationManager.AppSettings.Get("SDI_AUTO_FLUSH"));

        public FORM_CONFIG()
        {
            InitializeComponent();
            apic_id_Box.Text = apic_id;
            apic_pw_Box.Text = apic_pw;
            line_token_Box.Text = line_token;
            if (SDI_AUTO_TIMER == 1) AutoScan_Box.CheckState = CheckState.Checked;
            if (SDI_AUTO_FLUSH == 1) AutoFlush_Box.CheckState = CheckState.Checked;
        }

        private void UPDATE_button_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (confirmResult == DialogResult.Yes)
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                config.AppSettings.Settings["APIC_ID"].Value = apic_id_Box.Text;
                config.AppSettings.Settings["APIC_PASS"].Value = apic_pw_Box.Text;
                config.AppSettings.Settings["Line_token"].Value = line_token_Box.Text;

                if (AutoScan_Box.CheckState == CheckState.Checked)
                    config.AppSettings.Settings["SDI_AUTO_TIMER"].Value = "1";
                else
                    config.AppSettings.Settings["SDI_AUTO_TIMER"].Value = "0";

                if (AutoFlush_Box.CheckState == CheckState.Checked)
                    config.AppSettings.Settings["SDI_AUTO_FLUSH"].Value = "1";
                else
                    config.AppSettings.Settings["SDI_AUTO_FLUSH"].Value = "0";

                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
                MessageBox.Show("OK");
            }
        }
    }
}
