﻿namespace ACI_Flush_ARP_ENDPOINT
{
    partial class FORM_CONFIG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.AutoFlush_Box = new System.Windows.Forms.CheckBox();
            this.UPDATE_button = new System.Windows.Forms.Button();
            this.line_token_label = new System.Windows.Forms.Label();
            this.apic_id_Box = new System.Windows.Forms.TextBox();
            this.apic_pw_label = new System.Windows.Forms.Label();
            this.apic_pw_Box = new System.Windows.Forms.TextBox();
            this.apic_id_label = new System.Windows.Forms.Label();
            this.line_token_Box = new System.Windows.Forms.TextBox();
            this.apicIP_label = new System.Windows.Forms.Label();
            this.AutoScan_Box = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.AutoScan_Box);
            this.groupBox1.Controls.Add(this.AutoFlush_Box);
            this.groupBox1.Controls.Add(this.UPDATE_button);
            this.groupBox1.Controls.Add(this.line_token_label);
            this.groupBox1.Controls.Add(this.apic_id_Box);
            this.groupBox1.Controls.Add(this.apic_pw_label);
            this.groupBox1.Controls.Add(this.apic_pw_Box);
            this.groupBox1.Controls.Add(this.apic_id_label);
            this.groupBox1.Controls.Add(this.line_token_Box);
            this.groupBox1.Controls.Add(this.apicIP_label);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(460, 205);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SETTING";
            // 
            // AutoFlush_Box
            // 
            this.AutoFlush_Box.AutoSize = true;
            this.AutoFlush_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.AutoFlush_Box.Location = new System.Drawing.Point(16, 163);
            this.AutoFlush_Box.Name = "AutoFlush_Box";
            this.AutoFlush_Box.Size = new System.Drawing.Size(134, 29);
            this.AutoFlush_Box.TabIndex = 11;
            this.AutoFlush_Box.Text = "Auto Flush";
            this.AutoFlush_Box.UseVisualStyleBackColor = true;
            // 
            // UPDATE_button
            // 
            this.UPDATE_button.Location = new System.Drawing.Point(160, 130);
            this.UPDATE_button.Name = "UPDATE_button";
            this.UPDATE_button.Size = new System.Drawing.Size(288, 62);
            this.UPDATE_button.TabIndex = 10;
            this.UPDATE_button.Text = "UPDATE";
            this.UPDATE_button.UseVisualStyleBackColor = true;
            this.UPDATE_button.Click += new System.EventHandler(this.UPDATE_button_Click);
            // 
            // line_token_label
            // 
            this.line_token_label.AutoSize = true;
            this.line_token_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.line_token_label.Location = new System.Drawing.Point(12, 96);
            this.line_token_label.Name = "line_token_label";
            this.line_token_label.Size = new System.Drawing.Size(105, 24);
            this.line_token_label.TabIndex = 8;
            this.line_token_label.Text = "Line Token";
            // 
            // apic_id_Box
            // 
            this.apic_id_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apic_id_Box.Location = new System.Drawing.Point(160, 19);
            this.apic_id_Box.Name = "apic_id_Box";
            this.apic_id_Box.Size = new System.Drawing.Size(288, 29);
            this.apic_id_Box.TabIndex = 1;
            // 
            // apic_pw_label
            // 
            this.apic_pw_label.AutoSize = true;
            this.apic_pw_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apic_pw_label.Location = new System.Drawing.Point(12, 59);
            this.apic_pw_label.Name = "apic_pw_label";
            this.apic_pw_label.Size = new System.Drawing.Size(87, 24);
            this.apic_pw_label.TabIndex = 7;
            this.apic_pw_label.Text = "APIC PW";
            // 
            // apic_pw_Box
            // 
            this.apic_pw_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apic_pw_Box.Location = new System.Drawing.Point(160, 56);
            this.apic_pw_Box.Name = "apic_pw_Box";
            this.apic_pw_Box.Size = new System.Drawing.Size(288, 29);
            this.apic_pw_Box.TabIndex = 2;
            this.apic_pw_Box.UseSystemPasswordChar = true;
            // 
            // apic_id_label
            // 
            this.apic_id_label.AutoSize = true;
            this.apic_id_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apic_id_label.Location = new System.Drawing.Point(13, 22);
            this.apic_id_label.Name = "apic_id_label";
            this.apic_id_label.Size = new System.Drawing.Size(74, 24);
            this.apic_id_label.TabIndex = 6;
            this.apic_id_label.Text = "APIC ID";
            // 
            // line_token_Box
            // 
            this.line_token_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.line_token_Box.Location = new System.Drawing.Point(160, 93);
            this.line_token_Box.Name = "line_token_Box";
            this.line_token_Box.Size = new System.Drawing.Size(288, 29);
            this.line_token_Box.TabIndex = 3;
            // 
            // apicIP_label
            // 
            this.apicIP_label.AutoSize = true;
            this.apicIP_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apicIP_label.Location = new System.Drawing.Point(12, 22);
            this.apicIP_label.Name = "apicIP_label";
            this.apicIP_label.Size = new System.Drawing.Size(0, 25);
            this.apicIP_label.TabIndex = 5;
            // 
            // AutoScan_Box
            // 
            this.AutoScan_Box.AutoSize = true;
            this.AutoScan_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.AutoScan_Box.Location = new System.Drawing.Point(16, 130);
            this.AutoScan_Box.Name = "AutoScan_Box";
            this.AutoScan_Box.Size = new System.Drawing.Size(130, 29);
            this.AutoScan_Box.TabIndex = 12;
            this.AutoScan_Box.Text = "Auto Scan";
            this.AutoScan_Box.UseVisualStyleBackColor = true;
            // 
            // CONFIG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 226);
            this.Controls.Add(this.groupBox1);
            this.Name = "CONFIG";
            this.Text = "CONFIG";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox AutoFlush_Box;
        private System.Windows.Forms.Button UPDATE_button;
        private System.Windows.Forms.Label line_token_label;
        private System.Windows.Forms.TextBox apic_id_Box;
        private System.Windows.Forms.Label apic_pw_label;
        private System.Windows.Forms.TextBox apic_pw_Box;
        private System.Windows.Forms.Label apic_id_label;
        private System.Windows.Forms.TextBox line_token_Box;
        private System.Windows.Forms.Label apicIP_label;
        private System.Windows.Forms.CheckBox AutoScan_Box;
    }
}