﻿namespace ACI_Flush_ARP_ENDPOINT
{
    partial class FORM_MAIN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FORM_MAIN));
            this.IP_BOX = new System.Windows.Forms.TextBox();
            this.CHECK_IP_BUTTON = new System.Windows.Forms.Button();
            this.BGW_LEAF201 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF202 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF203 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF204 = new System.ComponentModel.BackgroundWorker();
            this.LAEF_TST_01_LED = new System.Windows.Forms.Button();
            this.LAEF_TST_02_LED = new System.Windows.Forms.Button();
            this.ToolTipLeaf = new System.Windows.Forms.ToolTip(this.components);
            this.LAEF_TST_03_LED = new System.Windows.Forms.Button();
            this.LAEF_TST_04_LED = new System.Windows.Forms.Button();
            this.BOX_TST = new System.Windows.Forms.GroupBox();
            this.LAEF_TST_08_LED = new System.Windows.Forms.Button();
            this.LAEF_TST_07_LED = new System.Windows.Forms.Button();
            this.LEAF208_Label = new System.Windows.Forms.Label();
            this.LEAF207_Label = new System.Windows.Forms.Label();
            this.LEAF206_Label = new System.Windows.Forms.Label();
            this.LEAF205_Label = new System.Windows.Forms.Label();
            this.LEAF204_Label = new System.Windows.Forms.Label();
            this.LEAF203_Label = new System.Windows.Forms.Label();
            this.LEAF202_Label = new System.Windows.Forms.Label();
            this.LEAF201_Label = new System.Windows.Forms.Label();
            this.LAEF_TST_06_LED = new System.Windows.Forms.Button();
            this.LAEF_TST_05_LED = new System.Windows.Forms.Button();
            this.BGW_LEAF205 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF206 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF401 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF402 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF403 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF404 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF405 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF406 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF407 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF408 = new System.ComponentModel.BackgroundWorker();
            this.LAEF_BTT_01_LED = new System.Windows.Forms.Button();
            this.LAEF_BTT_02_LED = new System.Windows.Forms.Button();
            this.LAEF_BTT_03_LED = new System.Windows.Forms.Button();
            this.LAEF_BTT_04_LED = new System.Windows.Forms.Button();
            this.LAEF_BTT_05_LED = new System.Windows.Forms.Button();
            this.LAEF_BTT_06_LED = new System.Windows.Forms.Button();
            this.LAEF_BTT_07_LED = new System.Windows.Forms.Button();
            this.LAEF_BTT_08_LED = new System.Windows.Forms.Button();
            this.BOX_BTT = new System.Windows.Forms.GroupBox();
            this.LAEF_SRB_07_LED = new System.Windows.Forms.Button();
            this.LAEF_SRB_08_LED = new System.Windows.Forms.Button();
            this.LEAF608_Label = new System.Windows.Forms.Label();
            this.LEAF607_Label = new System.Windows.Forms.Label();
            this.LEAF408_Label = new System.Windows.Forms.Label();
            this.LEAF407_Label = new System.Windows.Forms.Label();
            this.LEAF406_Label = new System.Windows.Forms.Label();
            this.LEAF405_Label = new System.Windows.Forms.Label();
            this.LEAF404_Label = new System.Windows.Forms.Label();
            this.LEAF403_Label = new System.Windows.Forms.Label();
            this.LEAF402_Label = new System.Windows.Forms.Label();
            this.LEAF401_Label = new System.Windows.Forms.Label();
            this.BGW_LEAF601 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF602 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF603 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF604 = new System.ComponentModel.BackgroundWorker();
            this.LAEF_SRB_01_LED = new System.Windows.Forms.Button();
            this.LAEF_SRB_02_LED = new System.Windows.Forms.Button();
            this.LAEF_SRB_03_LED = new System.Windows.Forms.Button();
            this.LAEF_SRB_04_LED = new System.Windows.Forms.Button();
            this.LEAF601_Label = new System.Windows.Forms.Label();
            this.LEAF602_Label = new System.Windows.Forms.Label();
            this.BOX_SRB = new System.Windows.Forms.GroupBox();
            this.LAEF_SRB_05_LED = new System.Windows.Forms.Button();
            this.LEAF606_Label = new System.Windows.Forms.Label();
            this.LEAF605_Label = new System.Windows.Forms.Label();
            this.LAEF_SRB_06_LED = new System.Windows.Forms.Button();
            this.LEAF604_Label = new System.Windows.Forms.Label();
            this.LEAF603_Label = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowSDIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dEBUGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SDN_Timer = new System.Windows.Forms.Timer(this.components);
            this.BGW_Auto_Check = new System.ComponentModel.BackgroundWorker();
            this.IP_LIST_BOX = new System.Windows.Forms.TextBox();
            this.AUTO_GROUP = new System.Windows.Forms.GroupBox();
            this.TEXTALERT = new System.Windows.Forms.TextBox();
            this.Runing_LED_laber = new System.Windows.Forms.Label();
            this.SDN_Log = new System.Windows.Forms.RichTextBox();
            this.Runing_LED = new System.Windows.Forms.Button();
            this.SDN_ProgressBar = new System.Windows.Forms.ProgressBar();
            this.Auto_Flush_led = new System.Windows.Forms.Button();
            this.Auto_Flush_led_label = new System.Windows.Forms.Label();
            this.SDI_status_led = new System.Windows.Forms.Button();
            this.SDI_status_led_label = new System.Windows.Forms.Label();
            this.Check_ProgressBar = new System.Windows.Forms.ProgressBar();
            this.Status_check_led = new System.Windows.Forms.Button();
            this.Check_IP_label = new System.Windows.Forms.Label();
            this.Run_LED_Timer = new System.Windows.Forms.Timer(this.components);
            this.Loader_Setting_Timer = new System.Windows.Forms.Timer(this.components);
            this.BGW_LEAF605 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF606 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF207 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF208 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF607 = new System.ComponentModel.BackgroundWorker();
            this.BGW_LEAF608 = new System.ComponentModel.BackgroundWorker();
            this.BOX_TST.SuspendLayout();
            this.BOX_BTT.SuspendLayout();
            this.BOX_SRB.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.AUTO_GROUP.SuspendLayout();
            this.SuspendLayout();
            // 
            // IP_BOX
            // 
            this.IP_BOX.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.IP_BOX.Location = new System.Drawing.Point(12, 31);
            this.IP_BOX.Name = "IP_BOX";
            this.IP_BOX.Size = new System.Drawing.Size(372, 26);
            this.IP_BOX.TabIndex = 0;
            this.IP_BOX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // CHECK_IP_BUTTON
            // 
            this.CHECK_IP_BUTTON.Location = new System.Drawing.Point(390, 31);
            this.CHECK_IP_BUTTON.Name = "CHECK_IP_BUTTON";
            this.CHECK_IP_BUTTON.Size = new System.Drawing.Size(157, 27);
            this.CHECK_IP_BUTTON.TabIndex = 1;
            this.CHECK_IP_BUTTON.Text = "CHECK IP";
            this.CHECK_IP_BUTTON.UseVisualStyleBackColor = true;
            this.CHECK_IP_BUTTON.Click += new System.EventHandler(this.CHECK_IP_Button_Click);
            // 
            // BGW_LEAF201
            // 
            this.BGW_LEAF201.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF201_DoWork);
            // 
            // BGW_LEAF202
            // 
            this.BGW_LEAF202.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF202_DoWork);
            // 
            // BGW_LEAF203
            // 
            this.BGW_LEAF203.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF203_DoWork);
            // 
            // BGW_LEAF204
            // 
            this.BGW_LEAF204.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF204_DoWork);
            // 
            // LAEF_TST_01_LED
            // 
            this.LAEF_TST_01_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_TST_01_LED.Location = new System.Drawing.Point(139, 19);
            this.LAEF_TST_01_LED.Name = "LAEF_TST_01_LED";
            this.LAEF_TST_01_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_TST_01_LED.TabIndex = 2;
            this.LAEF_TST_01_LED.UseVisualStyleBackColor = false;
            this.LAEF_TST_01_LED.Click += new System.EventHandler(this.LAEF_TST_01_LED_Click);
            // 
            // LAEF_TST_02_LED
            // 
            this.LAEF_TST_02_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_TST_02_LED.Location = new System.Drawing.Point(139, 50);
            this.LAEF_TST_02_LED.Name = "LAEF_TST_02_LED";
            this.LAEF_TST_02_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_TST_02_LED.TabIndex = 3;
            this.LAEF_TST_02_LED.UseVisualStyleBackColor = false;
            this.LAEF_TST_02_LED.Click += new System.EventHandler(this.LAEF_TST_02_LED_Click);
            // 
            // ToolTipLeaf
            // 
            this.ToolTipLeaf.IsBalloon = true;
            this.ToolTipLeaf.ShowAlways = true;
            // 
            // LAEF_TST_03_LED
            // 
            this.LAEF_TST_03_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_TST_03_LED.Location = new System.Drawing.Point(139, 81);
            this.LAEF_TST_03_LED.Name = "LAEF_TST_03_LED";
            this.LAEF_TST_03_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_TST_03_LED.TabIndex = 4;
            this.LAEF_TST_03_LED.UseVisualStyleBackColor = false;
            this.LAEF_TST_03_LED.Click += new System.EventHandler(this.LAEF_TST_03_LED_Click);
            // 
            // LAEF_TST_04_LED
            // 
            this.LAEF_TST_04_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_TST_04_LED.Location = new System.Drawing.Point(139, 112);
            this.LAEF_TST_04_LED.Name = "LAEF_TST_04_LED";
            this.LAEF_TST_04_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_TST_04_LED.TabIndex = 5;
            this.LAEF_TST_04_LED.UseVisualStyleBackColor = false;
            this.LAEF_TST_04_LED.Click += new System.EventHandler(this.LAEF_TST_04_LED_Click);
            // 
            // BOX_TST
            // 
            this.BOX_TST.Controls.Add(this.LAEF_TST_08_LED);
            this.BOX_TST.Controls.Add(this.LAEF_TST_07_LED);
            this.BOX_TST.Controls.Add(this.LEAF208_Label);
            this.BOX_TST.Controls.Add(this.LEAF207_Label);
            this.BOX_TST.Controls.Add(this.LEAF206_Label);
            this.BOX_TST.Controls.Add(this.LEAF205_Label);
            this.BOX_TST.Controls.Add(this.LEAF204_Label);
            this.BOX_TST.Controls.Add(this.LEAF203_Label);
            this.BOX_TST.Controls.Add(this.LEAF202_Label);
            this.BOX_TST.Controls.Add(this.LEAF201_Label);
            this.BOX_TST.Controls.Add(this.LAEF_TST_06_LED);
            this.BOX_TST.Controls.Add(this.LAEF_TST_05_LED);
            this.BOX_TST.Controls.Add(this.LAEF_TST_01_LED);
            this.BOX_TST.Controls.Add(this.LAEF_TST_04_LED);
            this.BOX_TST.Controls.Add(this.LAEF_TST_02_LED);
            this.BOX_TST.Controls.Add(this.LAEF_TST_03_LED);
            this.BOX_TST.Location = new System.Drawing.Point(12, 63);
            this.BOX_TST.Name = "BOX_TST";
            this.BOX_TST.Size = new System.Drawing.Size(174, 368);
            this.BOX_TST.TabIndex = 10;
            this.BOX_TST.TabStop = false;
            this.BOX_TST.Text = "SITE TST";
            // 
            // LAEF_TST_08_LED
            // 
            this.LAEF_TST_08_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_TST_08_LED.Location = new System.Drawing.Point(139, 330);
            this.LAEF_TST_08_LED.Name = "LAEF_TST_08_LED";
            this.LAEF_TST_08_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_TST_08_LED.TabIndex = 38;
            this.LAEF_TST_08_LED.UseVisualStyleBackColor = false;
            this.LAEF_TST_08_LED.Click += new System.EventHandler(this.LAEF_TST_08_LED_Click);
            // 
            // LAEF_TST_07_LED
            // 
            this.LAEF_TST_07_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_TST_07_LED.Location = new System.Drawing.Point(139, 299);
            this.LAEF_TST_07_LED.Name = "LAEF_TST_07_LED";
            this.LAEF_TST_07_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_TST_07_LED.TabIndex = 37;
            this.LAEF_TST_07_LED.UseVisualStyleBackColor = false;
            this.LAEF_TST_07_LED.Click += new System.EventHandler(this.LAEF_TST_07_LED_Click);
            // 
            // LEAF208_Label
            // 
            this.LEAF208_Label.AutoSize = true;
            this.LEAF208_Label.Location = new System.Drawing.Point(6, 336);
            this.LEAF208_Label.Name = "LEAF208_Label";
            this.LEAF208_Label.Size = new System.Drawing.Size(133, 13);
            this.LEAF208_Label.TabIndex = 19;
            this.LEAF208_Label.Text = "LEAF TST-08 (NODE232) ";
            // 
            // LEAF207_Label
            // 
            this.LEAF207_Label.AutoSize = true;
            this.LEAF207_Label.Location = new System.Drawing.Point(6, 305);
            this.LEAF207_Label.Name = "LEAF207_Label";
            this.LEAF207_Label.Size = new System.Drawing.Size(133, 13);
            this.LEAF207_Label.TabIndex = 18;
            this.LEAF207_Label.Text = "LEAF TST-07 (NODE231) ";
            // 
            // LEAF206_Label
            // 
            this.LEAF206_Label.AutoSize = true;
            this.LEAF206_Label.Location = new System.Drawing.Point(6, 191);
            this.LEAF206_Label.Name = "LEAF206_Label";
            this.LEAF206_Label.Size = new System.Drawing.Size(133, 13);
            this.LEAF206_Label.TabIndex = 17;
            this.LEAF206_Label.Text = "LEAF TST-06 (NODE212) ";
            // 
            // LEAF205_Label
            // 
            this.LEAF205_Label.AutoSize = true;
            this.LEAF205_Label.Location = new System.Drawing.Point(6, 160);
            this.LEAF205_Label.Name = "LEAF205_Label";
            this.LEAF205_Label.Size = new System.Drawing.Size(133, 13);
            this.LEAF205_Label.TabIndex = 16;
            this.LEAF205_Label.Text = "LEAF TST-05 (NODE211) ";
            // 
            // LEAF204_Label
            // 
            this.LEAF204_Label.AutoSize = true;
            this.LEAF204_Label.Location = new System.Drawing.Point(6, 118);
            this.LEAF204_Label.Name = "LEAF204_Label";
            this.LEAF204_Label.Size = new System.Drawing.Size(133, 13);
            this.LEAF204_Label.TabIndex = 15;
            this.LEAF204_Label.Text = "LEAF TST-04 (NODE204) ";
            // 
            // LEAF203_Label
            // 
            this.LEAF203_Label.AutoSize = true;
            this.LEAF203_Label.Location = new System.Drawing.Point(6, 87);
            this.LEAF203_Label.Name = "LEAF203_Label";
            this.LEAF203_Label.Size = new System.Drawing.Size(133, 13);
            this.LEAF203_Label.TabIndex = 14;
            this.LEAF203_Label.Text = "LEAF TST-03 (NODE203) ";
            // 
            // LEAF202_Label
            // 
            this.LEAF202_Label.AutoSize = true;
            this.LEAF202_Label.Location = new System.Drawing.Point(6, 56);
            this.LEAF202_Label.Name = "LEAF202_Label";
            this.LEAF202_Label.Size = new System.Drawing.Size(133, 13);
            this.LEAF202_Label.TabIndex = 13;
            this.LEAF202_Label.Text = "LEAF TST-02 (NODE202) ";
            // 
            // LEAF201_Label
            // 
            this.LEAF201_Label.AutoSize = true;
            this.LEAF201_Label.Location = new System.Drawing.Point(6, 25);
            this.LEAF201_Label.Name = "LEAF201_Label";
            this.LEAF201_Label.Size = new System.Drawing.Size(133, 13);
            this.LEAF201_Label.TabIndex = 12;
            this.LEAF201_Label.Text = "LEAF TST-01 (NODE201) ";
            // 
            // LAEF_TST_06_LED
            // 
            this.LAEF_TST_06_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_TST_06_LED.Location = new System.Drawing.Point(139, 185);
            this.LAEF_TST_06_LED.Name = "LAEF_TST_06_LED";
            this.LAEF_TST_06_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_TST_06_LED.TabIndex = 7;
            this.LAEF_TST_06_LED.UseVisualStyleBackColor = false;
            this.LAEF_TST_06_LED.Click += new System.EventHandler(this.LAEF_TST_06_LED_Click);
            // 
            // LAEF_TST_05_LED
            // 
            this.LAEF_TST_05_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_TST_05_LED.Location = new System.Drawing.Point(139, 154);
            this.LAEF_TST_05_LED.Name = "LAEF_TST_05_LED";
            this.LAEF_TST_05_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_TST_05_LED.TabIndex = 6;
            this.LAEF_TST_05_LED.UseVisualStyleBackColor = false;
            this.LAEF_TST_05_LED.Click += new System.EventHandler(this.LAEF_TST_05_LED_Click);
            // 
            // BGW_LEAF205
            // 
            this.BGW_LEAF205.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF205_DoWork);
            // 
            // BGW_LEAF206
            // 
            this.BGW_LEAF206.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF206_DoWork);
            // 
            // BGW_LEAF401
            // 
            this.BGW_LEAF401.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF401_DoWork);
            // 
            // BGW_LEAF402
            // 
            this.BGW_LEAF402.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF402_DoWork);
            // 
            // BGW_LEAF403
            // 
            this.BGW_LEAF403.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF403_DoWork);
            // 
            // BGW_LEAF404
            // 
            this.BGW_LEAF404.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF404_DoWork);
            // 
            // BGW_LEAF405
            // 
            this.BGW_LEAF405.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF405_DoWork);
            // 
            // BGW_LEAF406
            // 
            this.BGW_LEAF406.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF406_DoWork);
            // 
            // BGW_LEAF407
            // 
            this.BGW_LEAF407.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF407_DoWork);
            // 
            // BGW_LEAF408
            // 
            this.BGW_LEAF408.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF408_DoWork);
            // 
            // LAEF_BTT_01_LED
            // 
            this.LAEF_BTT_01_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_BTT_01_LED.Location = new System.Drawing.Point(140, 19);
            this.LAEF_BTT_01_LED.Name = "LAEF_BTT_01_LED";
            this.LAEF_BTT_01_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_BTT_01_LED.TabIndex = 8;
            this.LAEF_BTT_01_LED.UseVisualStyleBackColor = false;
            this.LAEF_BTT_01_LED.Click += new System.EventHandler(this.LAEF_BTT_01_LED_Click);
            // 
            // LAEF_BTT_02_LED
            // 
            this.LAEF_BTT_02_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_BTT_02_LED.Location = new System.Drawing.Point(140, 50);
            this.LAEF_BTT_02_LED.Name = "LAEF_BTT_02_LED";
            this.LAEF_BTT_02_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_BTT_02_LED.TabIndex = 9;
            this.LAEF_BTT_02_LED.UseVisualStyleBackColor = false;
            this.LAEF_BTT_02_LED.Click += new System.EventHandler(this.LAEF_BTT_02_LED_Click);
            // 
            // LAEF_BTT_03_LED
            // 
            this.LAEF_BTT_03_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_BTT_03_LED.Location = new System.Drawing.Point(140, 81);
            this.LAEF_BTT_03_LED.Name = "LAEF_BTT_03_LED";
            this.LAEF_BTT_03_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_BTT_03_LED.TabIndex = 10;
            this.LAEF_BTT_03_LED.UseVisualStyleBackColor = false;
            this.LAEF_BTT_03_LED.Click += new System.EventHandler(this.LAEF_BTT_03_LED_Click);
            // 
            // LAEF_BTT_04_LED
            // 
            this.LAEF_BTT_04_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_BTT_04_LED.Location = new System.Drawing.Point(140, 112);
            this.LAEF_BTT_04_LED.Name = "LAEF_BTT_04_LED";
            this.LAEF_BTT_04_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_BTT_04_LED.TabIndex = 11;
            this.LAEF_BTT_04_LED.UseVisualStyleBackColor = false;
            this.LAEF_BTT_04_LED.Click += new System.EventHandler(this.LAEF_BTT_04_LED_Click);
            // 
            // LAEF_BTT_05_LED
            // 
            this.LAEF_BTT_05_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_BTT_05_LED.Location = new System.Drawing.Point(140, 154);
            this.LAEF_BTT_05_LED.Name = "LAEF_BTT_05_LED";
            this.LAEF_BTT_05_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_BTT_05_LED.TabIndex = 12;
            this.LAEF_BTT_05_LED.UseVisualStyleBackColor = false;
            this.LAEF_BTT_05_LED.Click += new System.EventHandler(this.LAEF_BTT_05_LED_Click);
            // 
            // LAEF_BTT_06_LED
            // 
            this.LAEF_BTT_06_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_BTT_06_LED.Location = new System.Drawing.Point(140, 185);
            this.LAEF_BTT_06_LED.Name = "LAEF_BTT_06_LED";
            this.LAEF_BTT_06_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_BTT_06_LED.TabIndex = 13;
            this.LAEF_BTT_06_LED.UseVisualStyleBackColor = false;
            this.LAEF_BTT_06_LED.Click += new System.EventHandler(this.LAEF_BTT_06_LED_Click);
            // 
            // LAEF_BTT_07_LED
            // 
            this.LAEF_BTT_07_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_BTT_07_LED.Location = new System.Drawing.Point(140, 227);
            this.LAEF_BTT_07_LED.Name = "LAEF_BTT_07_LED";
            this.LAEF_BTT_07_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_BTT_07_LED.TabIndex = 14;
            this.LAEF_BTT_07_LED.UseVisualStyleBackColor = false;
            this.LAEF_BTT_07_LED.Click += new System.EventHandler(this.LAEF_BTT_07_LED_Click);
            // 
            // LAEF_BTT_08_LED
            // 
            this.LAEF_BTT_08_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_BTT_08_LED.Location = new System.Drawing.Point(140, 258);
            this.LAEF_BTT_08_LED.Name = "LAEF_BTT_08_LED";
            this.LAEF_BTT_08_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_BTT_08_LED.TabIndex = 15;
            this.LAEF_BTT_08_LED.UseVisualStyleBackColor = false;
            this.LAEF_BTT_08_LED.Click += new System.EventHandler(this.LAEF_BTT_08_LED_Click);
            // 
            // BOX_BTT
            // 
            this.BOX_BTT.Controls.Add(this.LEAF408_Label);
            this.BOX_BTT.Controls.Add(this.LEAF407_Label);
            this.BOX_BTT.Controls.Add(this.LEAF406_Label);
            this.BOX_BTT.Controls.Add(this.LEAF405_Label);
            this.BOX_BTT.Controls.Add(this.LEAF404_Label);
            this.BOX_BTT.Controls.Add(this.LEAF403_Label);
            this.BOX_BTT.Controls.Add(this.LEAF402_Label);
            this.BOX_BTT.Controls.Add(this.LEAF401_Label);
            this.BOX_BTT.Controls.Add(this.LAEF_BTT_04_LED);
            this.BOX_BTT.Controls.Add(this.LAEF_BTT_08_LED);
            this.BOX_BTT.Controls.Add(this.LAEF_BTT_01_LED);
            this.BOX_BTT.Controls.Add(this.LAEF_BTT_07_LED);
            this.BOX_BTT.Controls.Add(this.LAEF_BTT_02_LED);
            this.BOX_BTT.Controls.Add(this.LAEF_BTT_06_LED);
            this.BOX_BTT.Controls.Add(this.LAEF_BTT_03_LED);
            this.BOX_BTT.Controls.Add(this.LAEF_BTT_05_LED);
            this.BOX_BTT.Location = new System.Drawing.Point(193, 63);
            this.BOX_BTT.Name = "BOX_BTT";
            this.BOX_BTT.Size = new System.Drawing.Size(174, 368);
            this.BOX_BTT.TabIndex = 26;
            this.BOX_BTT.TabStop = false;
            this.BOX_BTT.Text = "SITE BTT";
            // 
            // LAEF_SRB_07_LED
            // 
            this.LAEF_SRB_07_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_SRB_07_LED.Location = new System.Drawing.Point(143, 299);
            this.LAEF_SRB_07_LED.Name = "LAEF_SRB_07_LED";
            this.LAEF_SRB_07_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_SRB_07_LED.TabIndex = 36;
            this.LAEF_SRB_07_LED.UseVisualStyleBackColor = false;
            this.LAEF_SRB_07_LED.Click += new System.EventHandler(this.LAEF_SRB_07_LED_Click);
            // 
            // LAEF_SRB_08_LED
            // 
            this.LAEF_SRB_08_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_SRB_08_LED.Location = new System.Drawing.Point(143, 330);
            this.LAEF_SRB_08_LED.Name = "LAEF_SRB_08_LED";
            this.LAEF_SRB_08_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_SRB_08_LED.TabIndex = 35;
            this.LAEF_SRB_08_LED.UseVisualStyleBackColor = false;
            this.LAEF_SRB_08_LED.Click += new System.EventHandler(this.LAEF_SRB_08_LED_Click);
            // 
            // LEAF608_Label
            // 
            this.LEAF608_Label.AutoSize = true;
            this.LEAF608_Label.Location = new System.Drawing.Point(9, 336);
            this.LEAF608_Label.Name = "LEAF608_Label";
            this.LEAF608_Label.Size = new System.Drawing.Size(131, 13);
            this.LEAF608_Label.TabIndex = 34;
            this.LEAF608_Label.Text = "LEAF-SRB-06 (NODE632)";
            // 
            // LEAF607_Label
            // 
            this.LEAF607_Label.AutoSize = true;
            this.LEAF607_Label.Location = new System.Drawing.Point(9, 305);
            this.LEAF607_Label.Name = "LEAF607_Label";
            this.LEAF607_Label.Size = new System.Drawing.Size(131, 13);
            this.LEAF607_Label.TabIndex = 33;
            this.LEAF607_Label.Text = "LEAF SRB-05 (NODE631)";
            // 
            // LEAF408_Label
            // 
            this.LEAF408_Label.AutoSize = true;
            this.LEAF408_Label.Location = new System.Drawing.Point(6, 264);
            this.LEAF408_Label.Name = "LEAF408_Label";
            this.LEAF408_Label.Size = new System.Drawing.Size(130, 13);
            this.LEAF408_Label.TabIndex = 32;
            this.LEAF408_Label.Text = "LEAF BTT-08 (NODE422)";
            // 
            // LEAF407_Label
            // 
            this.LEAF407_Label.AutoSize = true;
            this.LEAF407_Label.Location = new System.Drawing.Point(6, 233);
            this.LEAF407_Label.Name = "LEAF407_Label";
            this.LEAF407_Label.Size = new System.Drawing.Size(130, 13);
            this.LEAF407_Label.TabIndex = 31;
            this.LEAF407_Label.Text = "LEAF BTT-07 (NODE421)";
            // 
            // LEAF406_Label
            // 
            this.LEAF406_Label.AutoSize = true;
            this.LEAF406_Label.Location = new System.Drawing.Point(6, 191);
            this.LEAF406_Label.Name = "LEAF406_Label";
            this.LEAF406_Label.Size = new System.Drawing.Size(130, 13);
            this.LEAF406_Label.TabIndex = 30;
            this.LEAF406_Label.Text = "LEAF BTT-06 (NODE412)";
            // 
            // LEAF405_Label
            // 
            this.LEAF405_Label.AutoSize = true;
            this.LEAF405_Label.Location = new System.Drawing.Point(6, 160);
            this.LEAF405_Label.Name = "LEAF405_Label";
            this.LEAF405_Label.Size = new System.Drawing.Size(130, 13);
            this.LEAF405_Label.TabIndex = 29;
            this.LEAF405_Label.Text = "LEAF BTT-05 (NODE411)";
            // 
            // LEAF404_Label
            // 
            this.LEAF404_Label.AutoSize = true;
            this.LEAF404_Label.Location = new System.Drawing.Point(6, 118);
            this.LEAF404_Label.Name = "LEAF404_Label";
            this.LEAF404_Label.Size = new System.Drawing.Size(130, 13);
            this.LEAF404_Label.TabIndex = 28;
            this.LEAF404_Label.Text = "LEAF BTT-04 (NODE404)";
            // 
            // LEAF403_Label
            // 
            this.LEAF403_Label.AutoSize = true;
            this.LEAF403_Label.Location = new System.Drawing.Point(6, 87);
            this.LEAF403_Label.Name = "LEAF403_Label";
            this.LEAF403_Label.Size = new System.Drawing.Size(130, 13);
            this.LEAF403_Label.TabIndex = 27;
            this.LEAF403_Label.Text = "LEAF BTT-03 (NODE403)";
            // 
            // LEAF402_Label
            // 
            this.LEAF402_Label.AutoSize = true;
            this.LEAF402_Label.Location = new System.Drawing.Point(6, 56);
            this.LEAF402_Label.Name = "LEAF402_Label";
            this.LEAF402_Label.Size = new System.Drawing.Size(130, 13);
            this.LEAF402_Label.TabIndex = 26;
            this.LEAF402_Label.Text = "LEAF BTT-02 (NODE402)";
            // 
            // LEAF401_Label
            // 
            this.LEAF401_Label.AutoSize = true;
            this.LEAF401_Label.Location = new System.Drawing.Point(6, 25);
            this.LEAF401_Label.Name = "LEAF401_Label";
            this.LEAF401_Label.Size = new System.Drawing.Size(130, 13);
            this.LEAF401_Label.TabIndex = 18;
            this.LEAF401_Label.Text = "LEAF BTT-01 (NODE401)";
            // 
            // BGW_LEAF601
            // 
            this.BGW_LEAF601.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF601_DoWork);
            // 
            // BGW_LEAF602
            // 
            this.BGW_LEAF602.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF602_DoWork);
            // 
            // BGW_LEAF603
            // 
            this.BGW_LEAF603.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF603_DoWork);
            // 
            // BGW_LEAF604
            // 
            this.BGW_LEAF604.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF604_DoWork);
            // 
            // LAEF_SRB_01_LED
            // 
            this.LAEF_SRB_01_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_SRB_01_LED.Location = new System.Drawing.Point(143, 154);
            this.LAEF_SRB_01_LED.Name = "LAEF_SRB_01_LED";
            this.LAEF_SRB_01_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_SRB_01_LED.TabIndex = 16;
            this.LAEF_SRB_01_LED.UseVisualStyleBackColor = false;
            this.LAEF_SRB_01_LED.Click += new System.EventHandler(this.LAEF_SRB_01_LED_Click);
            // 
            // LAEF_SRB_02_LED
            // 
            this.LAEF_SRB_02_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_SRB_02_LED.Location = new System.Drawing.Point(143, 185);
            this.LAEF_SRB_02_LED.Name = "LAEF_SRB_02_LED";
            this.LAEF_SRB_02_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_SRB_02_LED.TabIndex = 17;
            this.LAEF_SRB_02_LED.UseVisualStyleBackColor = false;
            this.LAEF_SRB_02_LED.Click += new System.EventHandler(this.LAEF_SRB_02_LED_Click);
            // 
            // LAEF_SRB_03_LED
            // 
            this.LAEF_SRB_03_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_SRB_03_LED.Location = new System.Drawing.Point(143, 227);
            this.LAEF_SRB_03_LED.Name = "LAEF_SRB_03_LED";
            this.LAEF_SRB_03_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_SRB_03_LED.TabIndex = 18;
            this.LAEF_SRB_03_LED.UseVisualStyleBackColor = false;
            this.LAEF_SRB_03_LED.Click += new System.EventHandler(this.LAEF_SRB_03_LED_Click);
            // 
            // LAEF_SRB_04_LED
            // 
            this.LAEF_SRB_04_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_SRB_04_LED.Location = new System.Drawing.Point(143, 258);
            this.LAEF_SRB_04_LED.Name = "LAEF_SRB_04_LED";
            this.LAEF_SRB_04_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_SRB_04_LED.TabIndex = 19;
            this.LAEF_SRB_04_LED.UseVisualStyleBackColor = false;
            this.LAEF_SRB_04_LED.Click += new System.EventHandler(this.LAEF_SRB_04_LED_Click);
            // 
            // LEAF601_Label
            // 
            this.LEAF601_Label.AutoSize = true;
            this.LEAF601_Label.Location = new System.Drawing.Point(9, 160);
            this.LEAF601_Label.Name = "LEAF601_Label";
            this.LEAF601_Label.Size = new System.Drawing.Size(131, 13);
            this.LEAF601_Label.TabIndex = 31;
            this.LEAF601_Label.Text = "LEAF SRB-01 (NODE611)";
            // 
            // LEAF602_Label
            // 
            this.LEAF602_Label.AutoSize = true;
            this.LEAF602_Label.Location = new System.Drawing.Point(9, 191);
            this.LEAF602_Label.Name = "LEAF602_Label";
            this.LEAF602_Label.Size = new System.Drawing.Size(131, 13);
            this.LEAF602_Label.TabIndex = 32;
            this.LEAF602_Label.Text = "LEAF SRB-02 (NODE612)";
            // 
            // BOX_SRB
            // 
            this.BOX_SRB.Controls.Add(this.LAEF_SRB_08_LED);
            this.BOX_SRB.Controls.Add(this.LAEF_SRB_07_LED);
            this.BOX_SRB.Controls.Add(this.LEAF608_Label);
            this.BOX_SRB.Controls.Add(this.LAEF_SRB_05_LED);
            this.BOX_SRB.Controls.Add(this.LEAF607_Label);
            this.BOX_SRB.Controls.Add(this.LEAF606_Label);
            this.BOX_SRB.Controls.Add(this.LEAF605_Label);
            this.BOX_SRB.Controls.Add(this.LAEF_SRB_06_LED);
            this.BOX_SRB.Controls.Add(this.LEAF604_Label);
            this.BOX_SRB.Controls.Add(this.LEAF603_Label);
            this.BOX_SRB.Controls.Add(this.LAEF_SRB_01_LED);
            this.BOX_SRB.Controls.Add(this.LAEF_SRB_04_LED);
            this.BOX_SRB.Controls.Add(this.LEAF602_Label);
            this.BOX_SRB.Controls.Add(this.LAEF_SRB_03_LED);
            this.BOX_SRB.Controls.Add(this.LEAF601_Label);
            this.BOX_SRB.Controls.Add(this.LAEF_SRB_02_LED);
            this.BOX_SRB.Location = new System.Drawing.Point(373, 63);
            this.BOX_SRB.Name = "BOX_SRB";
            this.BOX_SRB.Size = new System.Drawing.Size(174, 368);
            this.BOX_SRB.TabIndex = 33;
            this.BOX_SRB.TabStop = false;
            this.BOX_SRB.Text = "SITE SRB";
            // 
            // LAEF_SRB_05_LED
            // 
            this.LAEF_SRB_05_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_SRB_05_LED.Location = new System.Drawing.Point(143, 19);
            this.LAEF_SRB_05_LED.Name = "LAEF_SRB_05_LED";
            this.LAEF_SRB_05_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_SRB_05_LED.TabIndex = 35;
            this.LAEF_SRB_05_LED.UseVisualStyleBackColor = false;
            this.LAEF_SRB_05_LED.Click += new System.EventHandler(this.LAEF_SRB_05_LED_Click);
            // 
            // LEAF606_Label
            // 
            this.LEAF606_Label.AutoSize = true;
            this.LEAF606_Label.Location = new System.Drawing.Point(9, 56);
            this.LEAF606_Label.Name = "LEAF606_Label";
            this.LEAF606_Label.Size = new System.Drawing.Size(131, 13);
            this.LEAF606_Label.TabIndex = 38;
            this.LEAF606_Label.Text = "LEAF SRB-02 (NODE602)";
            // 
            // LEAF605_Label
            // 
            this.LEAF605_Label.AutoSize = true;
            this.LEAF605_Label.Location = new System.Drawing.Point(9, 25);
            this.LEAF605_Label.Name = "LEAF605_Label";
            this.LEAF605_Label.Size = new System.Drawing.Size(131, 13);
            this.LEAF605_Label.TabIndex = 37;
            this.LEAF605_Label.Text = "LEAF SRB-01 (NODE601)";
            // 
            // LAEF_SRB_06_LED
            // 
            this.LAEF_SRB_06_LED.BackColor = System.Drawing.Color.Yellow;
            this.LAEF_SRB_06_LED.Location = new System.Drawing.Point(143, 50);
            this.LAEF_SRB_06_LED.Name = "LAEF_SRB_06_LED";
            this.LAEF_SRB_06_LED.Size = new System.Drawing.Size(25, 25);
            this.LAEF_SRB_06_LED.TabIndex = 36;
            this.LAEF_SRB_06_LED.UseVisualStyleBackColor = false;
            this.LAEF_SRB_06_LED.Click += new System.EventHandler(this.LAEF_SRB_06_LED_Click);
            // 
            // LEAF604_Label
            // 
            this.LEAF604_Label.AutoSize = true;
            this.LEAF604_Label.Location = new System.Drawing.Point(9, 264);
            this.LEAF604_Label.Name = "LEAF604_Label";
            this.LEAF604_Label.Size = new System.Drawing.Size(131, 13);
            this.LEAF604_Label.TabIndex = 34;
            this.LEAF604_Label.Text = "LEAF SRB-04 (NODE622)";
            // 
            // LEAF603_Label
            // 
            this.LEAF603_Label.AutoSize = true;
            this.LEAF603_Label.Location = new System.Drawing.Point(9, 233);
            this.LEAF603_Label.Name = "LEAF603_Label";
            this.LEAF603_Label.Size = new System.Drawing.Size(131, 13);
            this.LEAF603_Label.TabIndex = 33;
            this.LEAF603_Label.Text = "LEAF SRB-03 (NODE621)";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(984, 24);
            this.menuStrip1.TabIndex = 34;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ShowSDIToolStripMenuItem,
            this.configToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // ShowSDIToolStripMenuItem
            // 
            this.ShowSDIToolStripMenuItem.Name = "ShowSDIToolStripMenuItem";
            this.ShowSDIToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.ShowSDIToolStripMenuItem.Text = "Toggle SDI Zone";
            this.ShowSDIToolStripMenuItem.Click += new System.EventHandler(this.ShowSDIToolStripMenuItem_Click);
            // 
            // configToolStripMenuItem
            // 
            this.configToolStripMenuItem.Name = "configToolStripMenuItem";
            this.configToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.configToolStripMenuItem.Text = "Config";
            this.configToolStripMenuItem.Click += new System.EventHandler(this.ConfigToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dEBUGToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(40, 20);
            this.toolStripMenuItem1.Text = "###";
            // 
            // dEBUGToolStripMenuItem
            // 
            this.dEBUGToolStripMenuItem.Name = "dEBUGToolStripMenuItem";
            this.dEBUGToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.dEBUGToolStripMenuItem.Text = "Force RUN SDI";
            this.dEBUGToolStripMenuItem.Click += new System.EventHandler(this.DEBUGToolStripMenuItem_Click);
            // 
            // SDN_Timer
            // 
            this.SDN_Timer.Enabled = true;
            this.SDN_Timer.Interval = 180000;
            this.SDN_Timer.Tick += new System.EventHandler(this.SDN_Timer_Tick);
            // 
            // BGW_Auto_Check
            // 
            this.BGW_Auto_Check.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_Auto_Check_DoWork);
            // 
            // IP_LIST_BOX
            // 
            this.IP_LIST_BOX.Cursor = System.Windows.Forms.Cursors.Default;
            this.IP_LIST_BOX.Location = new System.Drawing.Point(9, 19);
            this.IP_LIST_BOX.Multiline = true;
            this.IP_LIST_BOX.Name = "IP_LIST_BOX";
            this.IP_LIST_BOX.ReadOnly = true;
            this.IP_LIST_BOX.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.IP_LIST_BOX.Size = new System.Drawing.Size(287, 100);
            this.IP_LIST_BOX.TabIndex = 36;
            this.IP_LIST_BOX.TabStop = false;
            // 
            // AUTO_GROUP
            // 
            this.AUTO_GROUP.Controls.Add(this.TEXTALERT);
            this.AUTO_GROUP.Controls.Add(this.Runing_LED_laber);
            this.AUTO_GROUP.Controls.Add(this.SDN_Log);
            this.AUTO_GROUP.Controls.Add(this.Runing_LED);
            this.AUTO_GROUP.Controls.Add(this.SDN_ProgressBar);
            this.AUTO_GROUP.Controls.Add(this.Auto_Flush_led);
            this.AUTO_GROUP.Controls.Add(this.Auto_Flush_led_label);
            this.AUTO_GROUP.Controls.Add(this.IP_LIST_BOX);
            this.AUTO_GROUP.Controls.Add(this.SDI_status_led);
            this.AUTO_GROUP.Controls.Add(this.SDI_status_led_label);
            this.AUTO_GROUP.Location = new System.Drawing.Point(577, 35);
            this.AUTO_GROUP.Name = "AUTO_GROUP";
            this.AUTO_GROUP.Size = new System.Drawing.Size(400, 428);
            this.AUTO_GROUP.TabIndex = 37;
            this.AUTO_GROUP.TabStop = false;
            this.AUTO_GROUP.Text = "SDN ZONE";
            // 
            // TEXTALERT
            // 
            this.TEXTALERT.AcceptsReturn = true;
            this.TEXTALERT.AcceptsTab = true;
            this.TEXTALERT.Location = new System.Drawing.Point(289, 129);
            this.TEXTALERT.Multiline = true;
            this.TEXTALERT.Name = "TEXTALERT";
            this.TEXTALERT.ReadOnly = true;
            this.TEXTALERT.Size = new System.Drawing.Size(100, 84);
            this.TEXTALERT.TabIndex = 48;
            this.TEXTALERT.Visible = false;
            // 
            // Runing_LED_laber
            // 
            this.Runing_LED_laber.AutoSize = true;
            this.Runing_LED_laber.Location = new System.Drawing.Point(302, 29);
            this.Runing_LED_laber.Name = "Runing_LED_laber";
            this.Runing_LED_laber.Size = new System.Drawing.Size(68, 13);
            this.Runing_LED_laber.TabIndex = 46;
            this.Runing_LED_laber.Text = "SDI Running";
            // 
            // SDN_Log
            // 
            this.SDN_Log.Cursor = System.Windows.Forms.Cursors.Default;
            this.SDN_Log.Location = new System.Drawing.Point(9, 129);
            this.SDN_Log.Name = "SDN_Log";
            this.SDN_Log.ReadOnly = true;
            this.SDN_Log.Size = new System.Drawing.Size(380, 267);
            this.SDN_Log.TabIndex = 45;
            this.SDN_Log.TabStop = false;
            this.SDN_Log.Text = "";
            this.SDN_Log.TextChanged += new System.EventHandler(this.Auto_Log_TextChanged);
            // 
            // Runing_LED
            // 
            this.Runing_LED.BackColor = System.Drawing.Color.Green;
            this.Runing_LED.Location = new System.Drawing.Point(369, 25);
            this.Runing_LED.Name = "Runing_LED";
            this.Runing_LED.Size = new System.Drawing.Size(20, 20);
            this.Runing_LED.TabIndex = 44;
            this.Runing_LED.TabStop = false;
            this.Runing_LED.UseVisualStyleBackColor = false;
            // 
            // SDN_ProgressBar
            // 
            this.SDN_ProgressBar.Location = new System.Drawing.Point(9, 402);
            this.SDN_ProgressBar.Name = "SDN_ProgressBar";
            this.SDN_ProgressBar.Size = new System.Drawing.Size(380, 20);
            this.SDN_ProgressBar.TabIndex = 37;
            // 
            // Auto_Flush_led
            // 
            this.Auto_Flush_led.BackColor = System.Drawing.Color.Green;
            this.Auto_Flush_led.Location = new System.Drawing.Point(369, 82);
            this.Auto_Flush_led.Name = "Auto_Flush_led";
            this.Auto_Flush_led.Size = new System.Drawing.Size(20, 20);
            this.Auto_Flush_led.TabIndex = 43;
            this.Auto_Flush_led.TabStop = false;
            this.Auto_Flush_led.UseVisualStyleBackColor = false;
            // 
            // Auto_Flush_led_label
            // 
            this.Auto_Flush_led_label.AutoSize = true;
            this.Auto_Flush_led_label.Location = new System.Drawing.Point(302, 86);
            this.Auto_Flush_led_label.Name = "Auto_Flush_led_label";
            this.Auto_Flush_led_label.Size = new System.Drawing.Size(57, 13);
            this.Auto_Flush_led_label.TabIndex = 42;
            this.Auto_Flush_led_label.Text = "Auto Flush";
            // 
            // SDI_status_led
            // 
            this.SDI_status_led.BackColor = System.Drawing.Color.Green;
            this.SDI_status_led.Location = new System.Drawing.Point(369, 53);
            this.SDI_status_led.Name = "SDI_status_led";
            this.SDI_status_led.Size = new System.Drawing.Size(20, 20);
            this.SDI_status_led.TabIndex = 40;
            this.SDI_status_led.TabStop = false;
            this.SDI_status_led.UseVisualStyleBackColor = false;
            // 
            // SDI_status_led_label
            // 
            this.SDI_status_led_label.AutoSize = true;
            this.SDI_status_led_label.Location = new System.Drawing.Point(302, 57);
            this.SDI_status_led_label.Name = "SDI_status_led_label";
            this.SDI_status_led_label.Size = new System.Drawing.Size(54, 13);
            this.SDI_status_led_label.TabIndex = 41;
            this.SDI_status_led_label.Text = "SDI Timer";
            // 
            // Check_ProgressBar
            // 
            this.Check_ProgressBar.Location = new System.Drawing.Point(12, 443);
            this.Check_ProgressBar.Name = "Check_ProgressBar";
            this.Check_ProgressBar.Size = new System.Drawing.Size(429, 20);
            this.Check_ProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.Check_ProgressBar.TabIndex = 38;
            // 
            // Status_check_led
            // 
            this.Status_check_led.BackColor = System.Drawing.Color.Green;
            this.Status_check_led.Location = new System.Drawing.Point(527, 443);
            this.Status_check_led.Name = "Status_check_led";
            this.Status_check_led.Size = new System.Drawing.Size(20, 20);
            this.Status_check_led.TabIndex = 39;
            this.Status_check_led.TabStop = false;
            this.Status_check_led.UseVisualStyleBackColor = false;
            // 
            // Check_IP_label
            // 
            this.Check_IP_label.AutoSize = true;
            this.Check_IP_label.Location = new System.Drawing.Point(447, 446);
            this.Check_IP_label.Name = "Check_IP_label";
            this.Check_IP_label.Size = new System.Drawing.Size(81, 13);
            this.Check_IP_label.TabIndex = 44;
            this.Check_IP_label.Text = "Check Running";
            // 
            // Run_LED_Timer
            // 
            this.Run_LED_Timer.Enabled = true;
            this.Run_LED_Timer.Interval = 500;
            this.Run_LED_Timer.Tick += new System.EventHandler(this.Run_LED_Timer_Tick);
            // 
            // Loader_Setting_Timer
            // 
            this.Loader_Setting_Timer.Enabled = true;
            this.Loader_Setting_Timer.Interval = 5000;
            this.Loader_Setting_Timer.Tick += new System.EventHandler(this.Loader_Setting_Timer_Tick);
            // 
            // BGW_LEAF605
            // 
            this.BGW_LEAF605.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF605_DoWork);
            // 
            // BGW_LEAF606
            // 
            this.BGW_LEAF606.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF606_DoWork);
            // 
            // BGW_LEAF207
            // 
            this.BGW_LEAF207.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF207_DoWork);
            // 
            // BGW_LEAF208
            // 
            this.BGW_LEAF208.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF208_DoWork);
            // 
            // BGW_LEAF607
            // 
            this.BGW_LEAF607.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF607_DoWork);
            // 
            // BGW_LEAF608
            // 
            this.BGW_LEAF608.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGW_LEAF608_DoWork);
            // 
            // FORM_MAIN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 476);
            this.Controls.Add(this.Check_IP_label);
            this.Controls.Add(this.Status_check_led);
            this.Controls.Add(this.Check_ProgressBar);
            this.Controls.Add(this.AUTO_GROUP);
            this.Controls.Add(this.BOX_SRB);
            this.Controls.Add(this.BOX_BTT);
            this.Controls.Add(this.BOX_TST);
            this.Controls.Add(this.CHECK_IP_BUTTON);
            this.Controls.Add(this.IP_BOX);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FORM_MAIN";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ACI CHECK | FLUSH ARP V.1.3.1";
            this.SizeChanged += new System.EventHandler(this.MAIN_SizeChanged);
            this.BOX_TST.ResumeLayout(false);
            this.BOX_TST.PerformLayout();
            this.BOX_BTT.ResumeLayout(false);
            this.BOX_BTT.PerformLayout();
            this.BOX_SRB.ResumeLayout(false);
            this.BOX_SRB.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.AUTO_GROUP.ResumeLayout(false);
            this.AUTO_GROUP.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox IP_BOX;
        private System.Windows.Forms.Button CHECK_IP_BUTTON;
        private System.ComponentModel.BackgroundWorker BGW_LEAF201;
        private System.ComponentModel.BackgroundWorker BGW_LEAF202;
        private System.ComponentModel.BackgroundWorker BGW_LEAF203;
        private System.ComponentModel.BackgroundWorker BGW_LEAF204;
        private System.Windows.Forms.Button LAEF_TST_01_LED;
        private System.Windows.Forms.Button LAEF_TST_02_LED;
        private System.Windows.Forms.ToolTip ToolTipLeaf;
        private System.Windows.Forms.Button LAEF_TST_03_LED;
        private System.Windows.Forms.Button LAEF_TST_04_LED;
        private System.Windows.Forms.GroupBox BOX_TST;
        private System.Windows.Forms.Button LAEF_TST_06_LED;
        private System.Windows.Forms.Button LAEF_TST_05_LED;
        private System.ComponentModel.BackgroundWorker BGW_LEAF205;
        private System.ComponentModel.BackgroundWorker BGW_LEAF206;
        private System.Windows.Forms.Label LEAF206_Label;
        private System.Windows.Forms.Label LEAF205_Label;
        private System.Windows.Forms.Label LEAF204_Label;
        private System.Windows.Forms.Label LEAF203_Label;
        private System.Windows.Forms.Label LEAF202_Label;
        private System.Windows.Forms.Label LEAF201_Label;
        private System.ComponentModel.BackgroundWorker BGW_LEAF401;
        private System.ComponentModel.BackgroundWorker BGW_LEAF402;
        private System.ComponentModel.BackgroundWorker BGW_LEAF403;
        private System.ComponentModel.BackgroundWorker BGW_LEAF404;
        private System.ComponentModel.BackgroundWorker BGW_LEAF405;
        private System.ComponentModel.BackgroundWorker BGW_LEAF406;
        private System.ComponentModel.BackgroundWorker BGW_LEAF407;
        private System.ComponentModel.BackgroundWorker BGW_LEAF408;
        private System.Windows.Forms.Button LAEF_BTT_01_LED;
        private System.Windows.Forms.Button LAEF_BTT_02_LED;
        private System.Windows.Forms.Button LAEF_BTT_03_LED;
        private System.Windows.Forms.Button LAEF_BTT_04_LED;
        private System.Windows.Forms.Button LAEF_BTT_05_LED;
        private System.Windows.Forms.Button LAEF_BTT_06_LED;
        private System.Windows.Forms.Button LAEF_BTT_07_LED;
        private System.Windows.Forms.Button LAEF_BTT_08_LED;
        private System.Windows.Forms.GroupBox BOX_BTT;
        private System.Windows.Forms.Label LEAF408_Label;
        private System.Windows.Forms.Label LEAF407_Label;
        private System.Windows.Forms.Label LEAF406_Label;
        private System.Windows.Forms.Label LEAF405_Label;
        private System.Windows.Forms.Label LEAF404_Label;
        private System.Windows.Forms.Label LEAF403_Label;
        private System.Windows.Forms.Label LEAF402_Label;
        private System.Windows.Forms.Label LEAF401_Label;
        private System.ComponentModel.BackgroundWorker BGW_LEAF601;
        private System.ComponentModel.BackgroundWorker BGW_LEAF602;
        private System.ComponentModel.BackgroundWorker BGW_LEAF603;
        private System.ComponentModel.BackgroundWorker BGW_LEAF604;
        private System.Windows.Forms.Button LAEF_SRB_01_LED;
        private System.Windows.Forms.Button LAEF_SRB_02_LED;
        private System.Windows.Forms.Button LAEF_SRB_03_LED;
        private System.Windows.Forms.Button LAEF_SRB_04_LED;
        private System.Windows.Forms.Label LEAF601_Label;
        private System.Windows.Forms.Label LEAF602_Label;
        private System.Windows.Forms.GroupBox BOX_SRB;
        private System.Windows.Forms.Label LEAF604_Label;
        private System.Windows.Forms.Label LEAF603_Label;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Timer SDN_Timer;
        private System.ComponentModel.BackgroundWorker BGW_Auto_Check;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dEBUGToolStripMenuItem;
        private System.Windows.Forms.TextBox IP_LIST_BOX;
        private System.Windows.Forms.GroupBox AUTO_GROUP;
        private System.Windows.Forms.ProgressBar SDN_ProgressBar;
        private System.Windows.Forms.ToolStripMenuItem ShowSDIToolStripMenuItem;
        private System.Windows.Forms.ProgressBar Check_ProgressBar;
        private System.Windows.Forms.Button Status_check_led;
        private System.Windows.Forms.Button SDI_status_led;
        private System.Windows.Forms.Label SDI_status_led_label;
        private System.Windows.Forms.Label Auto_Flush_led_label;
        private System.Windows.Forms.Button Auto_Flush_led;
        private System.Windows.Forms.ToolStripMenuItem configToolStripMenuItem;
        private System.Windows.Forms.Label Check_IP_label;
        private System.Windows.Forms.Button Runing_LED;
        private System.Windows.Forms.RichTextBox SDN_Log;
        private System.Windows.Forms.Label Runing_LED_laber;
        private System.Windows.Forms.Timer Run_LED_Timer;
        private System.Windows.Forms.Timer Loader_Setting_Timer;
        private System.Windows.Forms.Button LAEF_SRB_05_LED;
        private System.Windows.Forms.Label LEAF606_Label;
        private System.Windows.Forms.Label LEAF605_Label;
        private System.Windows.Forms.Button LAEF_SRB_06_LED;
        private System.ComponentModel.BackgroundWorker BGW_LEAF605;
        private System.ComponentModel.BackgroundWorker BGW_LEAF606;
        private System.Windows.Forms.TextBox TEXTALERT;
        private System.Windows.Forms.Button LAEF_TST_08_LED;
        private System.Windows.Forms.Button LAEF_TST_07_LED;
        private System.Windows.Forms.Label LEAF208_Label;
        private System.Windows.Forms.Label LEAF207_Label;
        private System.Windows.Forms.Button LAEF_SRB_07_LED;
        private System.Windows.Forms.Button LAEF_SRB_08_LED;
        private System.Windows.Forms.Label LEAF608_Label;
        private System.Windows.Forms.Label LEAF607_Label;
        private System.ComponentModel.BackgroundWorker BGW_LEAF207;
        private System.ComponentModel.BackgroundWorker BGW_LEAF208;
        private System.ComponentModel.BackgroundWorker BGW_LEAF607;
        private System.ComponentModel.BackgroundWorker BGW_LEAF608;
    }
}

