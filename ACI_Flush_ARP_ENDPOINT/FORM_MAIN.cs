﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Configuration;
using System.Collections.Specialized;

using Renci.SshNet;
using System.Net;
using System.IO;

using RestSharp;
using Newtonsoft;
using Newtonsoft.Json.Linq;

using System.Runtime.Remoting.Channels.Tcp;

namespace ACI_Flush_ARP_ENDPOINT
{
    public partial class FORM_MAIN : Form
    {
        #region GLOBAL VAR
        // VAR FORM ConfigurationManager
        string ID;
        string PW;
        string line_token;
        int SDI_AUTO_TIMER;
        int SDI_AUTO_FLUSH;
        int IfAlert = 0;

        //INIT VAR
        int Toggle = 0; //USE TO TOGGLE SIZE
        int SDN_Run_LED_Status = 0, Check_LED_Status = 0; //USE TO LED FLASHING
        #endregion

        private void Load_Setting_VAR()
        {
            //LOAD VAR
            ID = ConfigurationManager.AppSettings.Get("APIC_ID");
            PW = ConfigurationManager.AppSettings.Get("APIC_PASS");
            line_token = ConfigurationManager.AppSettings.Get("Line_token");
            SDI_AUTO_TIMER = int.Parse(ConfigurationManager.AppSettings.Get("SDI_AUTO_TIMER"));
            SDI_AUTO_FLUSH = int.Parse(ConfigurationManager.AppSettings.Get("SDI_AUTO_FLUSH"));

            //UPDATE LED STATUS SDI_AUTO_FLUSH
            if (SDI_AUTO_FLUSH == 1)
            {
                Auto_Flush_led.BackColor = Color.Green;
                ToolTipLeaf.SetToolTip(this.Auto_Flush_led, "AUTO FLUSH RUNNING");
            }
            else
            {
                Auto_Flush_led.BackColor = Color.Red;
                ToolTipLeaf.SetToolTip(this.Auto_Flush_led, "AUTO FLUSH NOT RUNNING");
            }

            //UPDATE LED STATUS AUTO TIMER
            if (SDI_AUTO_TIMER == 1)
            {
                SDN_Timer.Enabled = true;
                SDI_status_led.BackColor = Color.Green;
                ToolTipLeaf.SetToolTip(this.SDI_status_led, "AUTO TIMER RUNNING");
            }
            else
            {
                SDN_Timer.Enabled = false;
                SDI_status_led.BackColor = Color.Red;
                ToolTipLeaf.SetToolTip(this.SDI_status_led, "AUTO TIMER NOT RUNNING");
            }
        }

        public FORM_MAIN()
        {
            InitializeComponent();

            //LOCK SIZE
            this.Size = new Size(575, 550);

            //First LOAD SETTING;
            Load_Setting_VAR();

            //OPEN TCP PORT FOR WHATSUP CHECK
            TcpChannel channel = new TcpChannel(30303);
        }

        private void CHECK_IP_Button_Click(object sender, EventArgs e)
        {
            Check_ProgressBar.Value = 0;

            //IP TEXTBOX != NULL
            if (IP_BOX.Text != "")
            {
                bool ValidateIP = IPAddress.TryParse(IP_BOX.Text, out IPAddress ip);

                //Check ValidateIP
                if (ValidateIP)
                {
                    //Copy ValidateIP to TEXTBOX
                    IP_BOX.Text = ip.ToString();

                    //RUN BGW ALL LEAF
                    //TST
                    if (BGW_LEAF201.IsBusy != true)
                        BGW_LEAF201.RunWorkerAsync();

                    if (BGW_LEAF202.IsBusy != true)
                        BGW_LEAF202.RunWorkerAsync();

                    if (BGW_LEAF203.IsBusy != true)
                        BGW_LEAF203.RunWorkerAsync();

                    if (BGW_LEAF204.IsBusy != true)
                        BGW_LEAF204.RunWorkerAsync();

                    if (BGW_LEAF205.IsBusy != true)
                        BGW_LEAF205.RunWorkerAsync();

                    if (BGW_LEAF206.IsBusy != true)
                        BGW_LEAF206.RunWorkerAsync();

                    if (BGW_LEAF207.IsBusy != true)
                        BGW_LEAF207.RunWorkerAsync();

                    if (BGW_LEAF208.IsBusy != true)
                        BGW_LEAF208.RunWorkerAsync();
                    //////////////////////////////////////////

                    //BTT
                    if (BGW_LEAF401.IsBusy != true)
                        BGW_LEAF401.RunWorkerAsync();

                    if (BGW_LEAF402.IsBusy != true)
                        BGW_LEAF402.RunWorkerAsync();

                    if (BGW_LEAF403.IsBusy != true)
                        BGW_LEAF403.RunWorkerAsync();

                    if (BGW_LEAF404.IsBusy != true)
                        BGW_LEAF404.RunWorkerAsync();

                    if (BGW_LEAF405.IsBusy != true)
                        BGW_LEAF405.RunWorkerAsync();

                    if (BGW_LEAF406.IsBusy != true)
                        BGW_LEAF406.RunWorkerAsync();

                    if (BGW_LEAF407.IsBusy != true)
                        BGW_LEAF407.RunWorkerAsync();

                    if (BGW_LEAF408.IsBusy != true)
                        BGW_LEAF408.RunWorkerAsync();
                    //////////////////////////////////////////

                    //SRB
                    if (BGW_LEAF601.IsBusy != true)
                        BGW_LEAF601.RunWorkerAsync();

                    if (BGW_LEAF602.IsBusy != true)
                        BGW_LEAF602.RunWorkerAsync();

                    if (BGW_LEAF603.IsBusy != true)
                        BGW_LEAF603.RunWorkerAsync();

                    if (BGW_LEAF604.IsBusy != true)
                        BGW_LEAF604.RunWorkerAsync();

                    if (BGW_LEAF605.IsBusy != true)
                        BGW_LEAF605.RunWorkerAsync();

                    if (BGW_LEAF606.IsBusy != true)
                        BGW_LEAF606.RunWorkerAsync();

                    if (BGW_LEAF607.IsBusy != true)
                        BGW_LEAF607.RunWorkerAsync();

                    if (BGW_LEAF608.IsBusy != true)
                        BGW_LEAF608.RunWorkerAsync();
                    //////////////////////////////////////////
                }
                else
                    MessageBox.Show("This is not a valide ip address");
            }
            else
                MessageBox.Show("Please input valide ip address");
        }

        #region BGW_ALL_LEAF_CHECK_ARP
        private void BGW_LEAF201_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_TST_01_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_TST_01_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.221", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    LAEF_TST_01_LED.BeginInvoke(new MethodInvoker(delegate
                    {
                        LAEF_TST_01_LED.BackColor = Color.Yellow;
                    }));

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_TST_01_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_TST_01_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_TST_01_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_TST_01_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_TST_01_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_TST_01_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF202_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_TST_02_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_TST_02_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.222", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_TST_02_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_TST_02_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_TST_02_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_TST_02_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_TST_02_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_TST_02_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF203_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_TST_03_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_TST_03_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.223", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_TST_03_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_TST_03_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_TST_03_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_TST_03_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_TST_03_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_TST_03_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF204_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_TST_04_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_TST_04_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.224", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_TST_04_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_TST_04_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_TST_04_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_TST_04_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_TST_04_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_TST_04_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF205_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_TST_05_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_TST_05_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.201", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_TST_05_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_TST_05_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_TST_05_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_TST_05_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_TST_05_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_TST_05_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF206_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_TST_06_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_TST_06_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.202", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_TST_06_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_TST_06_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_TST_06_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_TST_06_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_TST_06_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_TST_06_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));

                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF401_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_BTT_01_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_BTT_01_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.241", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_BTT_01_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_BTT_01_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_BTT_01_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_BTT_01_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_BTT_01_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_BTT_01_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF402_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_BTT_02_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_BTT_02_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.242", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_BTT_02_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_BTT_02_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_BTT_02_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_BTT_02_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_BTT_02_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_BTT_02_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF403_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_BTT_03_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_BTT_03_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.243", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_BTT_03_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_BTT_03_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_BTT_03_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_BTT_03_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_BTT_03_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_BTT_03_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF404_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_BTT_04_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_BTT_04_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.244", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_BTT_04_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_BTT_04_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_BTT_04_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_BTT_04_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_BTT_04_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_BTT_04_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF405_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_BTT_05_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_BTT_05_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.245", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_BTT_05_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_BTT_05_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_BTT_05_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_BTT_05_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_BTT_05_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_BTT_05_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF406_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_BTT_06_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_BTT_06_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.246", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_BTT_06_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_BTT_06_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_BTT_06_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_BTT_06_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_BTT_06_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_BTT_06_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF407_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_BTT_07_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_BTT_07_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.233", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_BTT_07_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_BTT_07_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_BTT_07_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_BTT_07_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_BTT_07_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_BTT_07_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF408_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_BTT_08_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_BTT_08_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.234", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_BTT_08_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_BTT_08_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_BTT_08_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_BTT_08_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_BTT_08_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_BTT_08_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF601_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_SRB_01_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_SRB_01_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.161", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_SRB_01_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_SRB_01_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_SRB_01_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_SRB_01_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_SRB_01_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_SRB_01_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF602_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_SRB_02_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_SRB_02_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.162", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_SRB_02_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_SRB_02_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_SRB_02_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_SRB_02_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_SRB_02_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_SRB_02_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF603_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_SRB_03_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_SRB_03_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.163", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_SRB_03_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_SRB_03_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_SRB_03_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_SRB_03_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_SRB_03_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_SRB_03_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF604_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_SRB_04_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_SRB_04_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.164", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_SRB_04_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_SRB_04_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_SRB_04_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_SRB_04_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_SRB_04_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_SRB_04_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF605_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_SRB_05_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_SRB_05_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.167", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_SRB_05_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_SRB_05_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_SRB_05_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_SRB_05_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_SRB_05_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_SRB_05_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF606_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_SRB_06_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_SRB_06_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.166", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_SRB_06_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_SRB_06_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_SRB_06_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_SRB_06_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_SRB_06_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_SRB_06_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 4;
                        }));
                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }
        #endregion

        #region SDN-FUNCTION-ZONE
        private void SDN_Timer_Tick(object sender, EventArgs e)
        {
            //CHECK AND RUN BGW WORK
            if (BGW_Auto_Check.IsBusy != true)
                BGW_Auto_Check.RunWorkerAsync();

            //LINE ALERT
            LineNotify("AUTO FLUSH ARP RUN@" + DateTime.Now);
        }

        private void BGW_Auto_Check_DoWork(object sender, DoWorkEventArgs e)
        {
            int IP_Count_All = 0, IP_Count = 0;

            //READ File IP.txt
            string[] IP_List = System.IO.File.ReadAllLines(@"IP.txt");

            //CLEAN IP BOX
            IP_LIST_BOX.BeginInvoke(new MethodInvoker(delegate
            {
                IP_LIST_BOX.Text = "";
            }));

            //CLEAN TEXT ALERT
            IfAlert = 0 ;
            TEXTALERT.BeginInvoke(new MethodInvoker(delegate
            {
                TEXTALERT.Text = "";
            }));

            //FOREACH LIST IP TO BOX
            foreach (string IP_text in IP_List)
            {
                IP_LIST_BOX.BeginInvoke(new MethodInvoker(delegate
                {
                    IP_LIST_BOX.Text += IP_text + "\r\n";
                }));

                IP_Count_All++;

                SDN_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                {
                    SDN_ProgressBar.Value = 0;
                }));
            }

            //LOG TIME
            SDN_Log.BeginInvoke(new MethodInvoker(delegate
            {
                SDN_Log.Text += "START @ " + DateTime.Now + "\r\n";
            }));

            //FOREACH LIST IP TO CHECK
            foreach (string IP_text in IP_List)
            {
                bool ValidateIP = IPAddress.TryParse(IP_text, out IPAddress ip);
                if (ValidateIP)
                {
                    try
                    {
                        //RUN SSH CHECK ARP & FLUSH 

                        //SITE TST
                        Send_SSH(IP_text, "10.11.20.221", "LEAF-TST-01");
                        Send_SSH(IP_text, "10.11.20.222", "LEAF-TST-02");
                        Send_SSH(IP_text, "10.11.20.223", "LEAF-TST-03");
                        Send_SSH(IP_text, "10.11.20.224", "LEAF-TST-04");
                        Send_SSH(IP_text, "10.11.20.201", "LEAF-TST-05");
                        Send_SSH(IP_text, "10.11.20.202", "LEAF-TST-06");
                        Send_SSH(IP_text, "10.11.20.171", "LEAF-TST-07");
                        Send_SSH(IP_text, "10.11.20.172", "LEAF-TST-08");
                        /////////////////////////////////////////////////

                        //SITE BTT
                        Send_SSH(IP_text, "10.11.20.241", "LEAF-BTT-01");
                        Send_SSH(IP_text, "10.11.20.242", "LEAF-BTT-02");
                        Send_SSH(IP_text, "10.11.20.243", "LEAF-BTT-03");
                        Send_SSH(IP_text, "10.11.20.244", "LEAF-BTT-04");
                        Send_SSH(IP_text, "10.11.20.245", "LEAF-BTT-05");
                        Send_SSH(IP_text, "10.11.20.246", "LEAF-BTT-06");
                        Send_SSH(IP_text, "10.11.20.233", "LEAF-BTT-07");
                        Send_SSH(IP_text, "10.11.20.234", "LEAF-BTT-08");
                        /////////////////////////////////////////////////

                        //SITE SRB
                        Send_SSH(IP_text, "10.11.20.161", "LEAF-SRB-01");
                        Send_SSH(IP_text, "10.11.20.162", "LEAF-SRB-02");
                        Send_SSH(IP_text, "10.11.20.163", "LEAF-SRB-03");
                        Send_SSH(IP_text, "10.11.20.164", "LEAF-SRB-04");
                        Send_SSH(IP_text, "10.11.20.166", "LEAF-SRB-05");
                        Send_SSH(IP_text, "10.11.20.167", "LEAF-SRB-06");
                        Send_SSH(IP_text, "10.11.20.173", "LEAF-SRB-07");
                        Send_SSH(IP_text, "10.11.20.174", "LEAF-SRB-08");
                        /////////////////////////////////////////////////
                    }
                    catch
                    {
                        MessageBox.Show("Auto Check ERROR!");
                        LineNotify("AUTO FLUSH ARP ERROR!");
                    }

                    IP_Count++;

                    //UPDATE SDN ProgressBar
                    SDN_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                    {
                        SDN_ProgressBar.Value = (int)Math.Ceiling((IP_Count * 100.0) / IP_Count_All);
                    }));
                }
            }

            SDN_Log.BeginInvoke(new MethodInvoker(delegate
            {
                SDN_Log.Text += "END @ " + DateTime.Now + "\r\n";
            }));

            if(IfAlert == 1)
            {
                var AlertText = TEXTALERT.Text;
                AlertText = Regex.Replace(AlertText, "A{3}", "\\n");
                AlertText = AlertText.Remove(AlertText.Length - 1); //"n"
                AlertText = AlertText.Remove(AlertText.Length - 1); //"\"
                //MessageBox.Show("Alert Send " + AlertText);
                Samuttchai_API("ดำเนินการ Auto Flush ARP IP " + AlertText.ToString());
                LineNotify("ดำเนินการ Auto Flush ARP IP " + AlertText.ToString());
            }
        }

        private void Send_SSH(string IP, string IP_LEAF, string NAME_LEAF)
        {
            using (SshClient ssh = new SshClient(IP_LEAF, ID, PW))
            {
                try
                {
                    //CONNECT SSH TO LEAF AND RUN COMMAND
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP);
                    ssh.Disconnect();

                    //REGEX \n -> \r\n
                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");

                    //FOUND ARP
                    if (x != "\r\n\r\n")
                    {
                        //LOG FOUND AND OUTPUT RESULT
                        SDN_Log.BeginInvoke(new MethodInvoker(delegate
                        {
                            SDN_Log.Text += "Found ARP IP " + IP + " @" + NAME_LEAF + "\r\n" + x;
                        }));

                        //LINE ALERT AND OUT PUT RESULT
                        LineNotify("Found ARP IP " + IP + " @" + NAME_LEAF + "\r\n" + x);

                        //CHECK AUTO FLUSH
                        if (SDI_AUTO_FLUSH == 1)
                        {
                            // SEARCH MAC FORM RESULT
                            // ============================================================ //
                            var startTag_MAC = "MAC : ";
                            int startIndex_MAC = x.IndexOf(startTag_MAC) + startTag_MAC.Length;
                            int endIndex_MAC = x.IndexOf(" ", startIndex_MAC);
                            string MAC = x.Substring(startIndex_MAC, endIndex_MAC - startIndex_MAC);
                            // ============================================================ //

                            // SEARCH VRF NAME FORM RESULT
                            // ============================================================ //
                            var startTag_VRF = "VRF name : ";
                            int startIndex_VRF = x.IndexOf(startTag_VRF) + startTag_VRF.Length;
                            int endIndex_VRF = x.IndexOf("\r\n", startIndex_VRF);
                            string VRF = x.Substring(startIndex_VRF, endIndex_VRF - startIndex_VRF);
                            // ============================================================ //

                            // SEARCH EP FLAGS FORM RESULT
                            // ============================================================ //
                            var startTag_EP = "EP Flags : ";
                            int startIndex_EP = x.IndexOf(startTag_EP) + startTag_EP.Length;
                            int endIndex_EP = x.IndexOf("|", startIndex_EP);
                            string EP = x.Substring(startIndex_EP, endIndex_EP - startIndex_EP);
                            // ============================================================ //

                            // CHECK MAC AND EP_FLAG
                            if (MAC == "0000.0000.0000")
                            {
                                //SSH AND SEND COMMAND FLUSH
                                ssh.Connect();
                                ssh.RunCommand("vsh -c 'clear system internal epm endpoint key vrf " + VRF + " ip " + IP + "'");
                                ssh.Disconnect();

                                //LINE ALERT AFTER FLUSH
                                LineNotify("AUTO Flush ARP " + IP + "@" + NAME_LEAF);

                                IfAlert = 1;
                                TEXTALERT.BeginInvoke(new MethodInvoker(delegate
                                {
                                    TEXTALERT.Text += IP + "@" + NAME_LEAF + "AAA";
                                }));

                                //Samuttchai_API("ดำเนินการ Auto Flush ARP IP " + IP + "@" + NAME_LEAF);
                            }
                        }

                    }
                    //NOT FOUND ARP
                    else
                    {
                        SDN_Log.BeginInvoke(new MethodInvoker(delegate
                        {
                            SDN_Log.Text += "Not Found ARP IP " + IP + " @" + NAME_LEAF + "\r\n";
                        }));
                    }

                }
                catch (Exception e)
                {
                    LineNotify("Auto Check " + NAME_LEAF + " Error\n" + e);
                }
            }
        }
        #endregion

        #region STRIP-MUNU-ITEM
        private void ShowSDIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // TOGGLE WINDOWS SIZE
            if (Toggle == 0)
            {
                Toggle = 1;
                this.Size = new Size(1000, 100);
            }
            else
            {
                Toggle = 0;
                this.Size = new Size(575, 550);
            }
        }

        private void ConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // SHOW CONFIG DIALOG
            FORM_CONFIG CONFIG = new FORM_CONFIG();
            CONFIG.ShowDialog();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // MESSAGEBOX CONFIRM , EXIT APP
            var confirmResult = MessageBox.Show("Are you sure ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (confirmResult == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void DEBUGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // FORCE RUN SDN TO DEBUG
            SDN_Timer_Tick(null, null);
        }
        #endregion

        #region GUI-CHANGE
        private void Auto_Log_TextChanged(object sender, EventArgs e)
        {
            // AUTO SCROLL SDN LOG
            SDN_Log.SelectionStart = SDN_Log.Text.Length;
            SDN_Log.ScrollToCaret();
            if (SDN_Log.Text.Length > 10000)
                SDN_Log.Text = "";
        }

        private void MAIN_SizeChanged(object sender, EventArgs e)
        {
            // FIX LOCK SIZE
            if (Toggle == 0)
                this.Size = new Size(575, 515);
            else
                this.Size = new Size(1000, 515);
        }
        #endregion

        #region TIMER_Tick_Function
        private void Loader_Setting_Timer_Tick(object sender, EventArgs e)
        {
            // AUTO LOAD CONFIG
            Load_Setting_VAR();
        }

        private void Run_LED_Timer_Tick(object sender, EventArgs e)
        {
            // SDN LED FLASHING
            if (BGW_Auto_Check.IsBusy == true)
            {
                ToolTipLeaf.SetToolTip(this.Runing_LED, "IN PROGRESS");
                if (SDN_Run_LED_Status == 0)
                {
                    Runing_LED.BackColor = Color.YellowGreen;
                    SDN_Run_LED_Status = 1;
                }
                else
                {
                    Runing_LED.BackColor = Color.Green;
                    SDN_Run_LED_Status = 0;
                }
            }
            else
            {
                ToolTipLeaf.SetToolTip(this.Runing_LED, "NOT RUNNING");
                Runing_LED.BackColor = Color.Red;
            }

            // CHECK LED FLASHING
            if (BGW_LEAF201.IsBusy == true || BGW_LEAF202.IsBusy == true || BGW_LEAF203.IsBusy == true || BGW_LEAF204.IsBusy == true || BGW_LEAF205.IsBusy == true || BGW_LEAF206.IsBusy == true || BGW_LEAF207.IsBusy == true || BGW_LEAF208.IsBusy == true
             || BGW_LEAF401.IsBusy == true || BGW_LEAF402.IsBusy == true || BGW_LEAF403.IsBusy == true || BGW_LEAF404.IsBusy == true || BGW_LEAF405.IsBusy == true || BGW_LEAF406.IsBusy == true || BGW_LEAF407.IsBusy == true || BGW_LEAF408.IsBusy == true
             || BGW_LEAF601.IsBusy == true || BGW_LEAF602.IsBusy == true || BGW_LEAF603.IsBusy == true || BGW_LEAF604.IsBusy == true || BGW_LEAF605.IsBusy == true || BGW_LEAF606.IsBusy == true || BGW_LEAF607.IsBusy == true || BGW_LEAF608.IsBusy == true)
            {
                ToolTipLeaf.SetToolTip(this.Status_check_led, "IN PROGRESS");
                if (Check_LED_Status == 0)
                {
                    Status_check_led.BackColor = Color.YellowGreen;
                    Check_LED_Status = 1;
                }
                else
                {
                    Status_check_led.BackColor = Color.Green;
                    Check_LED_Status = 0;
                }
            }
            else
            {
                ToolTipLeaf.SetToolTip(this.Status_check_led, "NOT RUNNING");
                Status_check_led.BackColor = Color.Red;
            }
        }
        #endregion

        #region COPY_TO_CLIPBOARD-AND MANUAL FLUSH
        private void Send_SSH_MANUAL(string IP, string IP_LEAF, string NAME_LEAF)
        {
            using (SshClient ssh = new SshClient(IP_LEAF, ID, PW))
            {
                try
                {
                    //CONNECT SSH TO LEAF AND RUN COMMAND
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP);
                    ssh.Disconnect();

                    //REGEX \n -> \r\n
                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");

                    //FOUND ARP
                    if (x != "\r\n\r\n")
                    {
                        // SEARCH VRF NAME FORM RESULT
                        // ============================================================ //
                        var startTag_VRF = "VRF name : ";
                        int startIndex_VRF = x.IndexOf(startTag_VRF) + startTag_VRF.Length;
                        int endIndex_VRF = x.IndexOf("\r\n", startIndex_VRF);
                        string VRF = x.Substring(startIndex_VRF, endIndex_VRF - startIndex_VRF);
                        // ============================================================ //

                        //SSH AND SEND COMMAND FLUSH
                        ssh.Connect();
                        ssh.RunCommand("vsh -c 'clear system internal epm endpoint key vrf " + VRF + " ip " + IP + "'");
                        ssh.Disconnect();

                        //LINE ALERT AFTER FLUSH
                        LineNotify("Manual Flush ARP " + IP + "@" + NAME_LEAF);
                    }
                    //NOT FOUND ARP
                    else
                    {
                        MessageBox.Show("Not Found ARP IP " + IP + " @" + NAME_LEAF);
                    }

                }
                catch (Exception e)
                {
                    LineNotify("Manual Check " + NAME_LEAF + " Error\n" + e);
                }
            }
        }

        private void LAEF_TST_01_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_TST_01_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_TST_01_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.221", "LEAF-TST-01");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_TST_02_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_TST_02_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_TST_02_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.222", "LEAF-TST-02");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_TST_03_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_TST_03_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_TST_03_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.223", "LEAF-TST-03");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_TST_04_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_TST_04_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_TST_04_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.224", "LEAF-TST-04");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_TST_05_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_TST_05_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_TST_05_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.201", "LEAF-TST-05");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_TST_06_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_TST_06_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_TST_06_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.202", "LEAF-TST-06");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_BTT_01_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_BTT_01_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_BTT_01_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.241", "LEAF-BTT-01");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_BTT_02_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_BTT_02_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_BTT_02_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.242", "LEAF-BTT-02");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_BTT_03_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_BTT_03_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_BTT_03_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.243", "LEAF-BTT-03");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_BTT_04_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_BTT_04_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_BTT_04_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.244", "LEAF-BTT-04");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_BTT_05_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_BTT_05_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_BTT_05_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.245", "LEAF-BTT-05");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_BTT_06_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_BTT_06_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_BTT_06_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.246", "LEAF-BTT-06");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_BTT_07_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_BTT_07_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_BTT_07_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.233", "LEAF-BTT-07");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_BTT_08_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_BTT_08_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_BTT_08_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.234", "LEAF-BTT-08");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_SRB_01_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_SRB_01_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_SRB_01_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.161", "LEAF-SRB-01");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_SRB_02_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_SRB_02_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_SRB_02_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.162", "LEAF-SRB-02");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_SRB_03_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_SRB_03_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_SRB_03_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.163", "LEAF-SRB-03");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_SRB_04_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_SRB_04_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_SRB_04_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.164", "LEAF-SRB-04");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_SRB_05_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_SRB_05_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_SRB_05_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.167", "LEAF-SRB-05");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_SRB_06_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_SRB_06_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_SRB_06_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.166", "LEAF-SRB-06");
                MessageBox.Show("Finish");
            }
        }
        
        #endregion

        private int LineNotify(string msg)
        {
            // SEND API TO LINE NOTIFY SERVER USING RESTSHARP AND NEWTONSOFT

            RestRequest line_post;
            RestClient line_client = new RestClient
            {
                BaseUrl = new System.Uri("https://notify-api.line.me/api/notify")
            };
            ServicePointManager.ServerCertificateValidationCallback += (RestClient, certificate, chain, sslPolicyErrors) => true;

            line_post = new RestRequest(Method.POST); ;
            line_post.AddHeader("Authorization", string.Format("Bearer " + line_token));
            line_post.AddHeader("content-type", "application/x-www-form-urlencoded");
            line_post.AddParameter("message", msg);

            try
            {
                IRestResponse response = line_client.Execute(line_post);
                return int.Parse((JObject.Parse(response.Content)["status"].ToString()));
            }
            catch
            {
                MessageBox.Show("Can't connect to line server !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 1;
            }
        }

        private void BGW_LEAF207_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_TST_07_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_TST_07_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.171", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        this.LAEF_TST_07_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            this.LAEF_TST_07_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_TST_07_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 5;
                        }));
                        return;
                    }
                    else
                    {
                        this.LAEF_TST_07_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            this.LAEF_TST_07_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_TST_07_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 5;
                        }));

                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF208_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_TST_08_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_TST_08_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.172", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_TST_08_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_TST_08_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_TST_08_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 5;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_TST_08_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_TST_08_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_TST_08_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 5;
                        }));

                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF607_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_SRB_07_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_SRB_07_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.173", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_SRB_07_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_SRB_07_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_SRB_07_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 5;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_SRB_07_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_SRB_07_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_SRB_07_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 5;
                        }));

                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void BGW_LEAF608_DoWork(object sender, DoWorkEventArgs e)
        {
            LAEF_SRB_08_LED.BeginInvoke(new MethodInvoker(delegate
            {
                LAEF_SRB_08_LED.BackColor = Color.Yellow;
            }));

            using (SshClient ssh = new SshClient("10.11.20.174", ID, PW))
            {
                try
                {
                    ssh.Connect();
                    var result = ssh.RunCommand("show system internal epm endpoint ip " + IP_BOX.Text);
                    ssh.Disconnect();

                    string x = Regex.Replace(result.Result, "(?<!\r)\n", "\r\n");
                    if (x == "\r\n\r\n")
                    {
                        LAEF_SRB_08_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_SRB_08_LED.BackColor = Color.Green;
                            ToolTipLeaf.SetToolTip(this.LAEF_SRB_08_LED, "NOT FOUND");
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 5;
                        }));
                        return;
                    }
                    else
                    {
                        LAEF_SRB_08_LED.BeginInvoke(new MethodInvoker(delegate
                        {
                            LAEF_SRB_08_LED.BackColor = Color.Red;
                            ToolTipLeaf.SetToolTip(this.LAEF_SRB_08_LED, x);
                        }));

                        Check_ProgressBar.BeginInvoke(new MethodInvoker(delegate
                        {
                            Check_ProgressBar.Value += 5;
                        }));

                        return;
                    }
                }
                catch
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private void LAEF_TST_07_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_TST_07_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_TST_07_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.171", "LEAF-TST-07");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_TST_08_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_TST_08_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_TST_08_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.172", "LEAF-TST-08");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_SRB_07_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_SRB_07_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_SRB_07_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.173", "LEAF-SRB-07");
                MessageBox.Show("Finish");
            }
        }

        private void LAEF_SRB_08_LED_Click(object sender, EventArgs e)
        {
            if (ToolTipLeaf.GetToolTip(this.LAEF_SRB_08_LED) != "")
                Clipboard.SetText(ToolTipLeaf.GetToolTip(this.LAEF_SRB_08_LED));

            var confirmResult = MessageBox.Show("Are you sure to manual Flush ARP ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirmResult == DialogResult.Yes)
            {
                Send_SSH_MANUAL(IP_BOX.Text, "10.11.20.174", "LEAF-SRB-08");
                MessageBox.Show("Finish");
            }
        }

        private void Samuttchai_API(string msg)
        {
            //RestRequest Samuttchai_post;
            //RestClient Samuttchai_client = new RestClient();

            //Samuttchai_client.BaseUrl = new System.Uri("http://203.150.221.195/monitor/alert-line.php");
            //ServicePointManager.ServerCertificateValidationCallback += (RestClient, certificate, chain, sslPolicyErrors) => true;

            //Samuttchai_post = new RestRequest(Method.POST); ;
            //Samuttchai_post.AddHeader("content-type", "application/x-www-form-urlencoded");
            //Samuttchai_post.AddParameter("group", "alert-incident");
            //Samuttchai_post.AddParameter("msg", msg);

            //IRestResponse response = Samuttchai_client.Execute(Samuttchai_post);
        }
    }
}
